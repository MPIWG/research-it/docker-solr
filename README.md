# Docker setup for Solr

This is a Docker setup using docker-compose for a [Solr](https://lucene.apache.org/solr/) service.

The service
* uses Solr 7 image from https://hub.docker.com/_/solr
* opens port 8983 on the host
* mounts shared directory `/mpiwg` into the container

# Running

* create an empty `data` directory. it will be owned by the "solr" user (uid=8983, gid=8983)

Start the container with
```
docker-compose up -d
```


